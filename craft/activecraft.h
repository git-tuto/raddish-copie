#ifndef ACTIVECRAFT_H
#define ACTIVECRAFT_H

#include "craft.h"

class ActiveCraft: public Craft
{
    Q_OBJECT

public:
    ActiveCraft(QWidget* parent = nullptr);

public slots:
    void setActive(Witch* witch);

protected:
    void createPresetButton();

private:
    Witch* active_;
};

#endif // ACTIVECRAFT_H
