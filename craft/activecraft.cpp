#include "activecraft.h"

ActiveCraft::ActiveCraft(QWidget* parent): Craft(parent),
    active_{ nullptr }
{
    strokeColor_ = Qt::green;
    scroll_->setColor(strokeColor_);

    witch_.setRune(Rune::beauty());
}

void ActiveCraft::setActive(Witch* witch)
{
    if (witch == active_)
        return;

    active_ = witch;

    if (active_)
        witch_ = *active_;
    else
        witch_.clearWand();

    scroll_->refresh();
}
