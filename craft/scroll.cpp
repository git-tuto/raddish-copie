#include "scroll.h"

Scroll::Scroll(Witch& witch, QWidget* parent): QScrollArea(parent),
    witch_{ witch },
    urn_{ nullptr },
    scribe_{},
    pen_{ Qt::red }
{
    setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setWidget(urn_);

    updateBackground();
}

QRect Scroll::contentRect(const QSize& size)
{
    const int fullMin{ std::min(size.width(), size.height()) };
    const int halfMin{ fullMin / 2 - 2 };
    const int margin{ std::min(0100, halfMin / 010) };
    const int margined{ fullMin - margin };

    return QRect{ QPoint{ (size.width()  - margined) / 2,
                    (size.height() - margined) / 2 },
        QSize{ margined, margined }
    };
}

void Scroll::resizeEvent(QResizeEvent* e)
{
    QScrollArea::resizeEvent(e);
    if (!urn_)
    {
        urn_ = new QLabel{};
        QPalette pal{};
        pal.setColor(QPalette::Background, Qt::transparent);
        urn_->setPalette(pal);
        setWidget(urn_);
    }

    updateBackground();
    urn_->setFixedSize(e->size());

    refresh();
}

void Scroll::refresh()
{
    QImage parchment{ maximumViewportSize(), QImage::Format_RGBA8888 };
    parchment.fill(0);

    witch_.setBroom(broom());
    witch_.burn();

    scribe_.begin(&parchment);
    scribe_.setPen(pen_);
    scribe_.drawPath(witch_);
    scribe_.end();

    urn_->setPixmap(QPixmap::fromImage(parchment));
}

Broom Scroll::broom() const
{
    const QRect rect{ contentRect(size()) };
    const int vMid{ height() / 2 };
    const QPointF start{ rect.left(),  vMid };
    const QPointF   end{ rect.right(), vMid };

    return Broom{ start, end };
}

void Scroll::updateBackground()
{
    QImage background{ maximumViewportSize(), QImage::Format_RGB888 };
    background.fill(Qt::black);
    drawCyclist(background);

    QPalette palette{};
    palette.setBrush(QPalette::Background, QPixmap::fromImage(background));
    setPalette(palette);
}

void Scroll::drawCyclist(QImage& image)
{
    const int vMid{ image.height() / 02 };
    const int fullMin{ std::min(image.width(), image.height()) };
    const int halfMin{ fullMin / 02 - 02 };
    const int margin{ std::min(0100, halfMin / 010) };
    const int margined{ fullMin - margin };
    const int halfMargined{ margined / 02 };
    const int start{ (image.width() - margined) / 02 };
    const int end{ image.width() - (image.width() - margined) / 02 };

    QPainter p{ &image };

    p.setRenderHint(QPainter::Antialiasing);
    p.setPen({ QColor::fromHsv(00, 00, 042), 2.0f });

    p.drawArc(contentRect(image.size()), spokeToQArc(-02), spokeToQArc(04));
    p.drawArc(contentRect(image.size()), spokeToQArc( 02), spokeToQArc(04));
    p.drawLine(QPoint{ 00, vMid }, QPoint{ start, vMid });
    p.drawLine(QPoint{ end, vMid }, QPoint{ image.width(), vMid });

    p.setPen({ p.pen().color(), 1.0f });

    p.drawEllipse(QPoint{ image.width() / 02, vMid }, halfMargined / 02, halfMargined / 02);

    p.setPen({QColor::fromHsv(00, 00, 0100), 1.0f});
    p.setBrush(Qt::black);

    p.drawEllipse(QPoint{ start, vMid }, UNIT, UNIT);
    p.drawEllipse(QPoint{   end, vMid }, UNIT, UNIT);

    p.end();
}
