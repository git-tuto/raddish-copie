#ifndef CRAFT_H
#define CRAFT_H

#include <QVBoxLayout>
#include <QToolBar>
#include <QToolButton>
#include <QMenu>

#include "scroll.h"
#include "presetbutton.h"

class Craft: public QWidget
{
    Q_OBJECT

public:
    Craft(QWidget* parent = nullptr);

private slots:
    void setRune(Rune r);
    void createPresetButton();

protected:
    Witch witch_;

    Scroll* scroll_;
    QToolBar* toolBar_;
    QColor strokeColor_;
};

#endif // CRAFT_H
