TEMPLATE = app
TARGET = raddish
QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG  += c++11
DEFINES += QT_DEPRECATED_WARNINGS

LIBS += -L../.build-Witch-Desktop-Release/ -lWitch
DEPENDPATH  += witch
INCLUDEPATH += witch craft pot instruments

SOURCES += \
    icons.cpp \
    main.cpp \
    mainwindow.cpp \
    pot/pot.cpp \
    pot/brew.cpp \
    pot/spatula.cpp \
    pot/cook.cpp \
    pot/hatch.cpp \
    craft/craft.cpp \
    craft/scroll.cpp \
    craft/presetbutton.cpp \
    craft/activecraft.cpp \
    instruments/instrument.cpp \
    instruments/movein.cpp \
    instruments/drawin.cpp

HEADERS += \
    defs.h \
    icons.h \
    mainwindow.h \
    pot/pot.h \
    pot/brew.h \
    pot/spatula.h \
    pot/cook.h \
    pot/hatch.h \
    craft/craft.h \
    craft/scroll.h \
    craft/presetbutton.h \
    craft/activecraft.h \
    instruments/instrument.h \
    instruments/movein.h \
    instruments/drawin.h

RESOURCES += \
    resources.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

