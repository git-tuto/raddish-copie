#ifndef ICONS_H
#define ICONS_H

#include "witch.h"
#include "defs.h"
#include <QIcon>
#include <QPainter>
#include <unordered_map>

#define ICON_SIZE 060

using Runicon = std::unordered_map<Rune*, QIcon>;

struct Icon
{

    static QIcon preset(Preset preset, const QColor& col);
    static QIcon tool(Tool tool, bool active = false);
    static QIcon rune(Rune r);
    static Runicon runes_;

private:
    static QImage emptyIcon(QSize size = { ICON_SIZE, ICON_SIZE })
    {
        QImage image{ size , QImage::Format_RGBA8888 };
        image.fill(0);
        return image;
    }

};


#endif // ICONS_H
