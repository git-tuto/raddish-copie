#include "hatch.h"

Hatch::Hatch(const Witch& witch): QPixmap(),
    witch_{ witch },
    ed_{ nullptr }
{
    update();
}

Hatch::Hatch(Witch* witch): QPixmap(),
    witch_{ *witch },
    ed_{ witch }
{
    update();
}

void Hatch::update()
{
}
