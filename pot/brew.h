#ifndef BREW_H
#define BREW_H

#include <Witch>

#include <QImage>

class Pot;

class Brew: public QImage
{
    friend class Cook;

public:
    Brew(const Brew* brew);
    Brew(const QSize& size, Pot* pot = nullptr);
    Brew(Pot* pot);

    Mass& mass() { return mass_; }
    void addWitch(Witch* w);

    void redraw(const QRect& rect);
    void redraw() { redraw(QRect{ {}, size() }); }

    Brew novel(const QRect& rect);
    Brew stagnant(const QRect& rect);

    void resizzle();
    void clear();

private:
    Pot* pot_;
    Mass mass_;
};


#endif // BREW_H
