#ifndef COOK_H
#define COOK_H

#include <QPainter>

class Witch;
class Brew;

#define SPECK 2
enum Cheff{ CHEFF, SIZZLER, PARTIE };

class Cook: public QPainter
{
public:
    Cook(Brew* b, Cheff c = CHEFF);

    Brew* brew();
    void offset(const QPoint& off);
    static bool resizzle(Brew& b);
    static bool toss(Brew& b);

    static void drawWitch(Brew* b, const Witch& w,
                      const QPen& s = Qt::NoPen, const QBrush& f = Qt::NoBrush);
    static void garnish(Brew* b);
    static QRect drawSpeck(Brew* b, const QPointF& at, float r = SPECK);

    static QRect speckRect(const QPoint& at);
};

#endif // COOK_H
