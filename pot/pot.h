#ifndef POT_H
#define POT_H

#include "brew.h"
#include "spatula.h"
#include "hatch.h"

#include <QMouseEvent>

#include <QWidget>

namespace Colour {
static const QColor chartreuse{ 160, 255, 23 };
}

class Pot: public QWidget
{
    Q_OBJECT

public:
    Pot(QWidget* parent = nullptr);

    const QTransform& view() const { return view_; }
    void pan()
    {
        const QPoint move{ mapFromGlobal(QCursor::pos()) - spat_.pos() };
        view_.translate(move.x(), move.y());
        QCursor::setPos(mapToGlobal(spat_.pos()));

        main_.redraw();
        garnish_.clear();

        repaint();
    }
    void zoom(float d)
    {
        const float z{ 1 + d };
        QTransform zoom{};
        zoom.translate(-spat_.pos().x() * d,
                       -spat_.pos().y() * d);
        zoom.scale(z, z);

        view_ = view_ * zoom;

        QCursor::setPos(mapToGlobal(spat_.pos()));
        main_.redraw();
        repaint();
    }

    void resizeEvent(QResizeEvent* e) override;
    void paintEvent(QPaintEvent* e) override;
    void leaveEvent(QEvent* e) override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void wheelEvent(QWheelEvent* e) override
    {
        zoom(e->delta() * 0.00042f);
    }

signals:
    void activeChanged(Witch* witch);

private:
    QTransform view_;
    QImage grid_;

    Brew main_;
    Brew garnish_;

    Spatula spat_;
    Hatch* hatch_;

    Witch* hover_;
    Witch* clicked_;
    Witch* active_;

    void hover(const QPoint& pos);

private slots:
    void updateSpatula(Qt::MouseButtons b = Qt::NoButton);
};

#endif // POT_H
