#include <QLayout>
#include <QTimer>
#include "cook.h"

#include "pot.h"

Pot::Pot(QWidget* parent): QWidget(parent),
    view_{},
    grid_{ QSize{ UNIT, UNIT }, QImage::Format_RGB888 },
    main_{ this },
    garnish_{ this },
    spat_{},
    hatch_{ nullptr },
    hover_{ nullptr },
    active_{ nullptr }
{
    grid_.fill(Qt::black);
    grid_.setPixelColor(QPoint{}, QColor::fromHsv(00, 00, 042));

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    setMouseTracking(true);
    setCursor(Qt::BlankCursor);

    QTimer::singleShot(2, this, SLOT(updateSpatula()));
/*
    const int witches{ 01000 };
    const int s{ 011 };
    QPointF at{ 420, 420 };
    QLineF last{ at - QPointF{ s, 0 },
                 at };
    float angle{ 0 };

    for (int p{0}; p < witches; ++p)
    {
        QMatrix m{};
        float lastAngle{ angle };
        angle += 135 - (random() % 270) - last.p1().y() * 0.01f;
        m.rotate(angle);
        float scale{ s + random() % (s * s * s) / s };
        m.scale(scale, scale);

        QPointF from{ last.p2() + m.map(QPointF{ 0.0f,  0.0f }) };
        QPointF   to{ from + m.map(QPointF{ 1.0f,  0.0f }) };
        Broom broom{ from, to };
        float direction{};

        if (!main_.mass().size())
        {
            main_.mass().push_back(Witcher::connect(
                                       from, to,
                                       (2 - (random() % UNIT) / 2.),
                                       (2 - (random() % UNIT) / 2.)));
        }
        else
        {
            main_.mass().push_back(Witcher::connect(
                                       main_.mass().back(), to,
                                       main_.mass().back().outAngle() + 1 - (random() % UNIT) / 4.));
        }

        last = broom;

        if (((p % 10) - 6) > random() % 4)
            last.setP2(lerp(last.p1(), at, 0.5f));
    }

    main_.redraw();
    */
}

void Pot::resizeEvent(QResizeEvent* e)
{
    main_.resizzle();
    garnish_.resizzle();
}

void Pot::paintEvent(QPaintEvent* e)
{
    QPainter p{ this };
    QRect r{ e->rect() };

    p.fillRect(r, grid_);
    p.drawImage(r, main_.copy(e->rect()));

    p.setCompositionMode(QPainter::CompositionMode_Difference);
    p.drawImage(r, garnish_.copy(r));

    if (underMouse())
    {
        p.drawPixmap(spat_.rect(), spat_);
    }
}

void Pot::leaveEvent(QEvent* e)
{
    if (hover_)
    {
        QRect rect{ hover_->rect() };
        hover_ = nullptr;
        garnish_.fill(0);

        repaint(rect);
    }

    repaint(spat_.rect());
    spat_.leave();
}

void Pot::mouseMoveEvent(QMouseEvent* e)
{
    Qt::MouseButtons snot{ e->buttons() };
    updateSpatula(snot);

    if (snot & Qt::MiddleButton)
    {
        pan();
        return;
    }

    hover(e->pos());
}

void Pot::hover(const QPoint& pos)
{
    if (spat_.dragged())
        return;

    QPoint mappedPos{ view().inverted().map(pos) };
    garnish_.clear();

    Witch* oldWitch{ hover_ };
    Witch* which{ nullptr };
    QRectF witchBounds{};

    for (Witch& w: main_.mass())
    {
        if (w.within(mappedPos))
        {
            witchBounds |= view().mapRect(Cook::speckRect(w.center().toPoint()));
//            garnish_.mass().push_back({ w.broom(), {} });

            if (!which)
                which = &w;
            else
            {
                float distA{ which->centerDistance(mappedPos) };
                float distB{ w.centerDistance(mappedPos) };

                if (distA > distB)
                    which = &w;
            }
        }

        if (oldWitch && oldWitch->bounds().intersects(w.bounds()))
        {
            witchBounds |= view().mapRect(Cook::speckRect(w.center().toPoint()));
        }
    }

    if (active_)
    {
        witchBounds |= view().mapRect(active_->rect());
        garnish_.mass().push_back(*active_);
    }

    if (which || which != oldWitch || !witchBounds.isEmpty())
    {
        hover_ = which;

        if (oldWitch)
            witchBounds |= view().mapRect(oldWitch->rect());

        if (which)
        {
            witchBounds |= view().mapRect(which->rect());
            garnish_.mass().push_back(*which);
        }

        Cook::garnish(&garnish_);
        repaint(witchBounds.toRect());
    }
}

void Pot::mousePressEvent(QMouseEvent* e)
{
    clicked_ = hover_;
}

void Pot::mouseReleaseEvent(QMouseEvent* e)
{
    updateSpatula(Qt::NoButton);

    if (hover_ == clicked_
     && hover_ != active_)
    {
        if (active_)
        {
            garnish_.redraw(active_->rect());
            repaint(active_->rect());
        }

        active_ = hover_;

        emit activeChanged(active_);
    }
}

void Pot::updateSpatula(Qt::MouseButtons b)
{
    if (b & Qt::MiddleButton)
    {
        spat_.setPos(spat_.pos(), Qt::MiddleButton);
        return;
    }

    QPoint roundedPos{ mapFromGlobal(QCursor::pos()) / UNIT * UNIT };

    if (b & Qt::RightButton)
    {
        spat_.setPos(spat_.pos(), Qt::RightButton);
        return;
    }

    QPoint oldPos{ spat_.pos()  };
    QRect oldRect{ spat_.rect() };

    if (b & Qt::RightButton)
    {
        spat_.setPos(roundedPos, Qt::LeftButton);
    }
    else
    {
        spat_.setPos(roundedPos, Qt::NoButton);
    }

    if (oldPos != roundedPos)
    {
        if (!spat_.out())
            repaint(oldRect | spat_.rect());
        else
            repaint(spat_.rect());
    }
}
