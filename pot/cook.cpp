#include "pot.h"

#include "cook.h"

Cook::Cook(Brew* b, Cheff c): QPainter(b)
{
    if (c != SIZZLER && b->pot_)
        setTransform(b->pot_->view());

    setRenderHint(QPainter::Antialiasing);
}

Brew* Cook::brew()
{
    return static_cast<Brew*>(device());
}

void Cook::offset(const QPoint& off)
{
    QTransform t{};
    t.translate(off.x(), off.y());

    Brew* b{ brew() };

    if (b)
    {
        if (b->pot_)
            setTransform(b->pot_->view() * t);
        else
            setTransform(t);
    }
}

bool Cook::resizzle(Brew& b)
{
    QSize s{ b.pot_->size() };
    QSize old{ b.size() };

    if (old.isEmpty()
        || s == old)
        return false;

    Brew boil{ b.pot_ };
    Cook c{ &boil, SIZZLER };
    c.drawImage(QPoint{}, b);
    c.end();
    b.swap(boil);

    if (s.width() > old.width())
        b.redraw(QRect{ QPoint{ old.width(), 0 },
                        QSize{  s.width() - old.width(),
                                s.height() }});

    if (s.height() > old.height())
        b.redraw(QRect{ QPoint{ 0, old.height() },
                        QSize{  s.width(),
                                s.height() - old.height() }});
    return true;
}

bool Cook::toss(Brew& b)
{
    QSize s{ b.pot_->size() };
    QSize old{ b.size() };

    if (old.isEmpty()
        || s == old)
        return false;

    Brew boil{ b.pot_ };
    Cook c{ &boil, SIZZLER };
    c.drawImage(QPoint{}, b);
    c.end();
    b.swap(boil);

    if (s.width() > old.width())
        b.redraw(QRect{ QPoint{ old.width(), 0 },
                        QSize{  s.width() - old.width(),
                                s.height() }});

    if (s.height() > old.height())
        b.redraw(QRect{ QPoint{ 0, old.height() },
                        QSize{  s.width(),
                                s.height() - old.height() }});
    return true;
}


void Cook::drawWitch(Brew* b, const Witch& w, const QPen& s, const QBrush& f)
{
    Cook c{ b };

    if (f.style() != Qt::NoBrush)
        c.fillPath(w, f);
    if (s.style() != Qt::NoPen)
        c.strokePath(w, s);

    c.end();
}

void Cook::garnish(Brew* b)
{
    b->fill(0);

    for (const Witch& w: b->mass())
    {
        drawSpeck(b, w.broom().p2(), UNIT/2);
        drawWitch(b, w, { Colour::chartreuse,  UNIT, Qt::SolidLine, Qt::FlatCap });
        drawWitch(b, w, { Qt::white,       UNIT - 3, Qt::SolidLine, Qt::FlatCap });
        drawSpeck(b, w.broom().p1(), UNIT/2);

        drawSpeck(b, w.center(), SPECK);
    }
}

QRect Cook::drawSpeck(Brew* b, const QPointF& at, float r)
{
    Cook c{ b };

//    if (fade)
//    {
//        float d{ QLineF{ mapFromGlobal(QCursor::pos()), at.toPoint() }.length() };
//        c.setOpacity(1 - d * d / 01000);
//    }

    c.setBrush({ Colour::chartreuse });
    c.drawEllipse(at, r + 1, r + 1);

    c.setBrush({ Qt::white });
    c.drawEllipse(at, r, r);
    c.end();

    return speckRect(at.toPoint());
}

QRect Cook::speckRect(const QPoint& at)
{
    return QRect{ at - QPoint{ 1 + SPECK, 1 + SPECK },
                  QSize{ 2 + SPECK * 2, 2 + SPECK * 2 } };
}
